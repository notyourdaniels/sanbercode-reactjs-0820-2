import React from "react";

let Converter = (props) => {
  return `${props.berat / 1000} kg`;
};

let Tugas10 = () => {
  let dataHargaBuah = [
    { nama: "Semangka", harga: 10000, berat: 1000 },
    { nama: "Anggur", harga: 40000, berat: 500 },
    { nama: "Strawberry", harga: 30000, berat: 400 },
    { nama: "Jeruk", harga: 30000, berat: 1000 },
    { nama: "Mangga", harga: 30000, berat: 500 },
  ];

  return (
    <div className="Main2">
      <h1 className="Header1">Tabel Harga Buah</h1>
      <table className="Table2">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{dataHargaBuah[0].nama}</td>
            <td>{dataHargaBuah[0].harga}</td>
            <td>
              <Converter berat={dataHargaBuah[0].berat} />
            </td>
          </tr>
          <tr>
            <td>{dataHargaBuah[1].nama}</td>
            <td>{dataHargaBuah[1].harga}</td>
            <td>
              <Converter berat={dataHargaBuah[1].berat} />
            </td>
          </tr>
          <tr>
            <td>{dataHargaBuah[2].nama}</td>
            <td>{dataHargaBuah[2].harga}</td>
            <td>
              <Converter berat={dataHargaBuah[2].berat} />
            </td>
          </tr>
          <tr>
            <td>{dataHargaBuah[3].nama}</td>
            <td>{dataHargaBuah[3].harga}</td>
            <td>
              <Converter berat={dataHargaBuah[3].berat} />
            </td>
          </tr>
          <tr>
            <td>{dataHargaBuah[4].nama}</td>
            <td>{dataHargaBuah[4].harga}</td>
            <td>
              <Converter berat={dataHargaBuah[4].berat} />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Tugas10;
