import React from "react";
let Tugas9 = () => {
  return (
    <div className="Main">
      <h1 className="Header1">Form Pembelian Buah</h1>
      <div className="Isian">
        <table>
          <tr>
            <td className="TulisanKiri">Nama Pelanggan</td>
            <td>
              <input type="text" />
            </td>
          </tr>
          <tr>
            <td className="TulisanKiri">Daftar Item</td>
            <td>
              <input type="checkbox" name="Semangka" />
              Semangka
              <br />
              <input type="checkbox" name="Jeruk" />
              Jeruk
              <br />
              <input type="checkbox" name="Nanas" />
              Nanas
              <br />
              <input type="checkbox" name="Salak" />
              Salak
              <br />
              <input type="checkbox" name="Anggur" />
              Anggur
            </td>
          </tr>
        </table>
        <input className="Kirim" type="submit" value="Kirim" />
      </div>
    </div>
  );
};

export default Tugas9;
